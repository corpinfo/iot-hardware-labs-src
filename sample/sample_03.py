from machine import Pin, PWM, ADC
from time import sleep
import math
import sys

frequency = 10000
ledR = PWM(Pin(23), frequency)
ledG = PWM(Pin(22), frequency)
ledB = PWM(Pin(21), frequency)

ledR.duty(0)
ledB.duty(0)
ledG.duty(0)
max_value = 4096


adc_pin = Pin(35)
adc = ADC(adc_pin)
while True:
    value = max_value - adc.read()

    # value - current reading
    # max_value - max value the value can ever read
    # 1023/10/10 - this is the range we set for the ledR/ledB/ledG
    ledR.duty(int(value / max_value * 1023))
    ledB.duty(int(value / max_value * 10))
    ledG.duty(int(value / max_value * 10))
