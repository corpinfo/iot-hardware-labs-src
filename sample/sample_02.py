from machine import Pin, PWM
from time import sleep
import math
import sys

frequency = 10000
ledR = PWM(Pin(23), frequency)
ledG = PWM(Pin(22), frequency)
ledB = PWM(Pin(21), frequency)

ledR.duty(0)
ledB.duty(0)
ledG.duty(0)

b_step = 1
r_step = 2
g_step = 3
b_cur = 0
g_cur = 0
r_cur = 0
while True:
    b_cur += b_step
    g_cur += g_step
    r_cur += r_step

    # if *_cur hit 100 we reset the value back to 0
    if b_cur > 100:
        b_cur = 0
    if g_cur > 100:
        g_cur = 0
    if r_cur > 100:
        r_cur = 0

    # *_cur - current value
    # 100 - max value *_cur can be
    # 1023/10/10 - this is the range we set for the ledR/ledB/ledG
    ledR.duty(int(math.sin(r_cur / 100 * math.pi) * 1023))
    ledG.duty(int(math.sin(g_cur / 100 * math.pi) * 10))
    ledB.duty(int(math.sin(b_cur / 100 * math.pi) * 10))
    sleep(0.02)
