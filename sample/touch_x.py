from machine import Pin, PWM, TouchPad
from time import sleep
import math
import sys

frequency = 10000
ledR = PWM(Pin(23), frequency)
ledG = PWM(Pin(22), frequency)
ledB = PWM(Pin(21), frequency)

touch = TouchPad(Pin(27))

ledR.duty(0)
ledB.duty(0)
ledG.duty(0)
max_value = 100

# sensitivity of the touch
touch.config(0)

while True:
    value = touch.read()
    print(value)

    # value - current reading
    # max_value - max value the value can ever read
    # 1023/10/10 - this is the range we set for the ledR/ledB/ledG
    ledR.duty(int(value / 100 * 1023))
    ledG.duty(int(value / 100 * 10))
    ledB.duty(int(value / 100 * 10))
