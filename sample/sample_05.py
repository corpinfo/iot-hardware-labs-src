from machine import Pin, PWM, ADC
from time import sleep
import math
import utime
from umqtt.simple import MQTTClient
import network
import ubinascii
import machine
import dht
import json

# the the MCU's unique_id - same client_id cannot be connected to the mqtt so we choose something that is unique.
client_id = ubinascii.hexlify(machine.unique_id()).decode('utf-8')

# WiFi configuration
ssid = 'onica-create'
password = 'wintogether'

# MQTT connection configurations
mqtt_server = 'avbi9uodfyzre-ats.iot.us-west-2.amazonaws.com'
topic = 'iot/{}/control'.format(client_id)
certificate = """
-----BEGIN CERTIFICATE-----
MIIDWjCCAkKgAwIBAgIVALvtdYvXeqZOlPnScC1FpPfLwmW6MA0GCSqGSIb3DQEB
CwUAME0xSzBJBgNVBAsMQkFtYXpvbiBXZWIgU2VydmljZXMgTz1BbWF6b24uY29t
IEluYy4gTD1TZWF0dGxlIFNUPVdhc2hpbmd0b24gQz1VUzAeFw0xOTA5MjQwMzM1
MDRaFw00OTEyMzEyMzU5NTlaMB4xHDAaBgNVBAMME0FXUyBJb1QgQ2VydGlmaWNh
dGUwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCiEj/hH5obA4mEpOAF
NPdJQVxQ9tskyWZZy8ksEEK0nIdOc+7Wtpn5YaMgsW894hjc4so4NFQUi86Ev0Z5
eTvvgQ05IC/ZcsNRm/THgDE7ha2Cgetgal2t6leidoAKFNGT6XxLyfK0r7EI9QJh
Ttu8IMMWKUZEdck0GJvIAqFj9/QqJL8G1dCJk2X3nJ+fRvvq6G7IFFg7ePsANkXA
bX5KEK60I30ubXyhth/ubg7yG5Rq7F4TxyElhzXMwFs46IFTmWambOlTxUsSHAdP
x84SBNtfRjwGDEWxBkL/lRcRMuQGxOiKkOrVvWvUL/x0zPVcDh3VDO+QMrggbV7S
Aa/PAgMBAAGjYDBeMB8GA1UdIwQYMBaAFPGkMirhJr5WplUQ6C88ghLRwhqXMB0G
A1UdDgQWBBSlqTu0ivrHdUeAQX/ETQxbKIihNDAMBgNVHRMBAf8EAjAAMA4GA1Ud
DwEB/wQEAwIHgDANBgkqhkiG9w0BAQsFAAOCAQEAJmozKe1ZKmjulpDNHNwgFlbY
+kg5mdeVFQRLU6BcRQPNShaoEO0n4ixr4OjCHH3Wajr1IGmKoWudCTPp1CV4OHbh
sZWvC2p8ren+rsZN0d9cUexMv00YNWwyPKSImdCKqK9EsQU+jdTgglpoPpEu0iLq
Jhj9JRSK/QXiZzFvjT9+kvpKfJGTn9HEIlxQvYG2e2+4KDW9q+fsfBrVXkEzk3Jw
oR6Ix51+rof/gG4Rp+0M8UN82hOPfGHgpS0gIJtmrP1g5kK2lRfgf3me2ssgllJW
oRGgHmQJZWlqlnftj7dqYXEf4mPAXPP5f3h3mZpGBJk6bQad9Jias8YrLTuK4Q==
-----END CERTIFICATE-----
"""
private = """
-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEAohI/4R+aGwOJhKTgBTT3SUFcUPbbJMlmWcvJLBBCtJyHTnPu
1raZ+WGjILFvPeIY3OLKODRUFIvOhL9GeXk774ENOSAv2XLDUZv0x4AxO4WtgoHr
YGpdrepXonaAChTRk+l8S8nytK+xCPUCYU7bvCDDFilGRHXJNBibyAKhY/f0KiS/
BtXQiZNl95yfn0b76uhuyBRYO3j7ADZFwG1+ShCutCN9Lm18obYf7m4O8huUauxe
E8chJYc1zMBbOOiBU5lmpmzpU8VLEhwHT8fOEgTbX0Y8BgxFsQZC/5UXETLkBsTo
ipDq1b1r1C/8dMz1XA4d1QzvkDK4IG1e0gGvzwIDAQABAoIBABRY/jQhZ5OkqgtB
N+7va2yDDDgOBg1kS7lSYHtrvzQ1wrcSzxWd707j9zsPi8vVKxHwVzsfNr8tnkwE
R7DQi7nSFfrqE5fVVLhWQDEb4xJ6AgK0lPyR3hkLptz6y7B4VKehVL2fKR2OQUfC
mv3kjKnd4yBRJkVKFGy5+jTuP5zxPUexMKgf5sC+l0gwh7OPKtyKWmc6bD0TRTGg
ZoZ1Fk4XEWKhDb4XD31ZgVD0/QURqrZv2uus6vb+fyrfiaSEUkN6TlOHVG2rv8io
I7QzKABFt8xk2J97i6zwouIvtKXxvRk81w2RBJSh4NqmCoSMMf9bxZ9YKGw8rWas
NEvhtSECgYEA0Xh1ksxrHgU66q8kkpH3bQK5OlU3G8OzUS6yveeMZ8WGHEB3Qq+A
ZT4bVEFeltNfg8CyRU0RKjjtie7pLI880WrYRdDbEaYr02m5RzEY8nM2qVmKoAxC
n5oVkFNt8CPj5NOFAsh9fzSjnRFfL2egeebFHnptIQTPNKttKOf9UwUCgYEAxhJu
kh1HA6PqNx7oT6GxrSh9JTdZlNhc7X40f4tbZtDJz4FpQtxdjDzeakbv68wjfg5U
77inEo44KEbrU99fj08JgPq6yfUHsAW6Wq4WpUCKfZ/JJJ1wMXZC94ga/e463YFY
h7idfx35pwtIHX334kUw8pAVbBb9qPAI9FMeF8MCgYEAlhxB/Wo6lS/gUdAjYFn0
Ow2JmdD5hg8PkOzrMIYfqzn06ogULxi/5U3FnfKei218dFmCJ4Gb/GsBZyhrtwAK
8HQl+po6c4Jmw8FvAiKgIxcHpALEAf/nOqLcMypmjYHazd/64My2ipM5cNTDJqCX
cuQ6fKX1UIU2zCLRPlBJzNUCgYBD9ZMGgxEYiKcV3n7qw5nIxbmaiVEtJsdLnR+6
G0BzGHQFxGsvS5i1QzU01b3/83zCBXzS+1InpbBS8XJQxrVs4Dtsi5qHLJFErdJw
xoZykKBz1d0XuQUDk4YKlCJeudxsn7KWbptrQAembtKBe/UPJ5pRv2n6OjEyQ5lu
6w3KLQKBgQCAeSnwk26qrK0cqByBmscE/t6dkaepSSRwAsv7j3kZjvObMRYEsVm+
TtvUJJfAd9iyUsWWx3mOelQ50SSi2wO98ZJZAfFMRXLJC6PaPqZyHj1H10M5aVko
NAXFkTIQGlz9+Whcf4wbIxH8Ul5kFo8Dh6LFTdx+PGJUDeWZNTibFg==
-----END RSA PRIVATE KEY-----
"""

frequency = 10000
ledR = PWM(Pin(23), frequency)
ledG = PWM(Pin(22), frequency)
ledB = PWM(Pin(21), frequency)

ledR.duty(0)
ledB.duty(0)
ledG.duty(0)

# Connect to the WiFi connection
sta_if = network.WLAN(network.STA_IF)
sta_if.active(True)
sta_if.connect(ssid, password)
while not sta_if.isconnected():
    utime.sleep(1)

adc_pin = Pin(36)
adc = ADC(adc_pin)

d = dht.DHT11(machine.Pin(33))
prev_temp = -1
prev_humidity = -1
prev_light = -1


def sub_cb(topic, msg):
    result = json.loads(msg)

    # *_cur - current value
    # 100 - max value *_cur can be
    # 1023/10/10 - this is the range we set for the ledR/ledB/ledG
    ledR.duty(int(result['r'] / 100 * 1023))
    ledG.duty(int(result['g'] / 100 * 10))
    ledB.duty(int(result['b'] / 100 * 10))


client = MQTTClient(
    client_id=client_id,
    server=mqtt_server,
    ssl=True,
    ssl_params={
        'key': private,
        'cert': certificate
    }
)
r = client.connect()

client.set_callback(sub_cb)
client.subscribe(topic)

while True:
    client.wait_msg()
