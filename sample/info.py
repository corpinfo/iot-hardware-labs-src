import machine
import ubinascii

client_id = ubinascii.hexlify(machine.unique_id()).decode('utf-8')
print('publish: iot/{}/sensor'.format(client_id))
print('subscribe: iot/{}/control'.format(client_id))