#!/usr/bin/env bash

# sets up the default port AMPY will use to execute the python sample on the MCU
export AMPY_PORT=$1

# drops you into shell with Pipfile dependencies
pipenv shell
